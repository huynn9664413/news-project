<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function __invoke($lang)
    {
        app()->setLocale($lang);
        session()->put('locale',$lang);
        return redirect()->back();
    }
}
