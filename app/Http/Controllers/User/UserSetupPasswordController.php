<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SetupPasswordRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserSetupPasswordController extends Controller
{
    public function index()
    {
        if (Auth::user()->password==null) {
            return view('user.setup-password');
        }
        return redirect()->route('homepage');
    }

    public function update(SetupPasswordRequest $request)
    {
        $password = $request->validated();
        User::find(Auth::id())->update($password);
        return redirect()->route('homepage');
    }
}
