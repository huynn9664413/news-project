<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserProfileController extends Controller
{
    public function profile(){
        $user = Auth::user();
        return view('user.profile',compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user){
        $updateData = $request->validated();
        $user->update($updateData);
        return redirect()->back();
    }
}
