<?php

namespace App\Http\Controllers;

use App\Enums\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthProviderController extends Controller
{
    public function redirect($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider){
        $socialUser = Socialite::driver($provider)->user();
        $user = User::firstOrCreate(
            [
                'email'=> $socialUser->getEmail(),
            ],
            [
                'name'=>$socialUser->getName(),
                'email'=>$socialUser->getEmail(),
                'role'=>UserRole::User,
            ]
            );
        Auth::login($user);
        return redirect()->route('homepage');
    }
}
