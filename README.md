# News Project



## Getting started

- Docker is a must before running the project

## Tech

1. **Backend**: Laravel.
2. **Frontend**: Blade.
3. **Additional package**: 
- Eloquent Filter
- Intervention Image

## Database

Run sail (or php) artisan db:seed --class=DatabaseSeeder to inject 3 account for 3 different role 

|Role     | Admin        | Writter        | User                |
|---------|--------------|----------------|---------------------|
|Account  |admin@news.com|writter@news.com|user@news.com        |
|Password |123           |123             |123                  |


## Roadmap

- ~~Socialite will be update in the future~~ (done)
- ~~Multilanguage using Laravel Localization~~ (done)
- Implement mail into project (working)
- Real-time using broadcast (working)
