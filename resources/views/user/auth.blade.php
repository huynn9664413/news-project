@extends('layout.main')
@section('content')
    <form action="{{route('pLogin')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">{{ __('news.user.email') }}</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{old('email')}}">
            @if ($errors->has('email'))
                @foreach ($errors->get('email') as $emailMessage)
                        <div>{{$emailMessage}}</div>
                @endforeach
            @endif
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">{{__('news.user.password')}}</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
            @if ($errors->has('password'))
                @foreach ($errors->get('password') as $passwordMessage)
                        <div>{{$passwordMessage}}</div>
                @endforeach
            @endif
        </div>
        <button type="submit" class="btn btn-primary">{{__('news.user.sign-in')}}</button>
        <div class="d-flex justify-content-start ">
            <a href="{{route('socialLogin','google')}}"><i class="fs-3 fa-brands fa-google"></i></a>
        </div>
        <a href="{{route('homepage')}}">
            <button type="button" class="btn btn-warning">{{__('news.redirect.to-homepage')}}</button>
        </a>
        <a href="{{route('register')}}">
            <button type="button" class="btn btn-info">{{__('news.user.sign-up')}}</button>
        </a>
        @if ($errors->has('msg'))
            <div>{{$errors->first('msg')}}</div>
        @endif
    </form>
@endsection
