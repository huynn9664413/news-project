@extends('layout.main')
@section('content')
<div class="container">
    <div>
        <h4>{{__('news.user.profile')}}</h4>
        <a>
            <button type="button" onclick="history.back()" class="btn btn-warning">{{__('news.redirect.back')}}</button>
        </a>
        <form action="{{route('user.update',$user->id)}}" method="post">
            @csrf
            @method('put')
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">{{__('news.user.name')}}</label>
                <input type="string" class="form-control" id="exampleInputEmail1" name="name" value="{{old('name')?old('name'):$user->name}}">
                @foreach ($errors->get('name') as $nameMessage)
                    <div>{{$nameMessage}}</div>
                @endforeach
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">{{__('news.user.email')}}</label>
                <input disabled type="email" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" name="email" value="{{$user->email}}">
                @foreach ($errors->get('email') as $emailMessage)
                    <div>{{$emailMessage}}</div>
                @endforeach
            </div>
            <button type="submit" class="btn btn-primary">{{__('news.user.send')}}</button>
        </form>
    </div>
    <hr></hr>
    <div>
        @if (Auth::user()->password !== null)
        <h4>{{__('news.plural.pass-change')}}</h4>
        <form action="{{route('user.update',$user->id)}}" method="post">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="exampleInputPassword1">{{__('news.user.old-pass')}}</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="old_password">
                @foreach ($errors->get('old_password') as $oldPasswordMessage)
                    <div class="text-danger">{{$oldPasswordMessage}}</div>
                @endforeach
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">{{__('news.user.new-pass')}}</label>
                <input type="password" class="form-control" id="exampleInputPassword2" name="password">
                @foreach ($errors->get('password') as $passwordMessage)
                    <div class="text-danger">{{$passwordMessage}}</div>
                @endforeach
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">{{__('news.user.new-pass-cf')}}</label>
                <input type="password" class="form-control" id="exampleInputPassword3" name="password_confirmation">
            </div>
            <button type="submit" class="btn btn-primary">{{__('news.user.send')}}</button>
        </form>
        @else
        <h4>{{__('news.plural.set-pass')}}</h4>
        <form action="{{route('pSetupPassword')}}">
            <div class="form-group">
                <label for="exampleInputPassword1">{{__('news.user.new-pass')}}</</label>
                <input type="password" class="form-control" id="exampleInputPassword2" name="password">
                @foreach ($errors->get('password') as $passwordMessage)
                    <div class="text-danger">{{$passwordMessage}}</div>
                @endforeach
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">{{__('news.user.new-pass-cf')}}</label>
                <input type="password" class="form-control" id="exampleInputPassword3" name="password_confirmation">
            </div>
            <button type="submit" class="btn btn-primary">{{__('news.user.send')}}</button>
        </form>
        @endif
    </div>
</div>
@endsection
