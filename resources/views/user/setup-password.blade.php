@extends('layout.main')
@section('content')
<form action="{{route('pSetupPassword')}}" method="post">
    @csrf
    <h4>{{__('news.plural.set-pass')}}</h4>
    <div class="form-group">
        <label for="exampleInputPassword1">{{__('news.user.new-pass')}}</label>
        <input type="password" class="form-control" id="exampleInputPassword2" name="password">
        @foreach ($errors->get('password') as $passwordMessage)
            <div class="text-danger">{{$passwordMessage}}</div>
        @endforeach
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">{{__('news.user.new-pass-cf')}}</label>
        <input type="password" class="form-control" id="exampleInputPassword3" name="password_confirmation">
    </div>
    <button type="submit" class="btn btn-primary">{{__('news.user.send')}}</button>
</form>
@endsection
