<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{__('news.plural.news')}}</title>
    @yield('style')
    @include('layout.css')
</head>
    <section>
    <!-- navbar -->
    @include('layout.nav')
    @auth
        @includeWhen(Auth::user()->password=='', 'layout.warning')
    @endauth

    </section>

    <section>
    <!-- content -->
    @yield('content')
    </section>


    <section>
    @include('layout.footer')
    </section>
    <section>
    <!-- script -->
    @include('layout.js')
    @yield('script')
    </section>
</body>
</html>
