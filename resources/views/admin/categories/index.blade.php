@extends('admin.homepage')
@section('content')
<h3>{{__('news.plural.category')}}</h3>
<hr>
<div>
    <a href="{{route('admin.categories.create')}}">
        <button type="button" class="btn btn-primary">{{__('news.user.create')}}</button>
    </a>
</div>
<div>
    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">{{__('news.plural.title')}}</th>
            <th scope="col">{{__('news.plural.description')}}</th>
            <th scope="col">{{__('news.plural.created')}}</th>
            <th scope="col">{{__('news.plural.updated')}}</th>
            <th scope="col">{{__('news.plural.deleted')}}</th>
            <th scope="col">{{__('news.plural.action')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $categories as $key=>$category )
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$category->category}}</td>
                <td>{{$category->description}}</td>
                <td>{{$category->created_at}}</td>
                <td>{{$category->updated_at}}</td>
                <td>{{$category->deleted_at?$category->deleted_at:''}}</td>
                <td class="d-flex">
                    <a href="{{route('admin.categories.edit',$category->id)}}">
                        <button type="button" class="btn btn-warning">{{__('news.user.edit')}}</button>
                    </a>
                    <form action="{{route('admin.categories.destroy',$category->id)}}" method="POST">
                        @csrf
                        @method('delete')
                        <button onclick="" type="submit" class="deleteBtn btn btn-danger">{{__('news.user.delete')}}</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('script')
<script>
$('.deleteBtn').click(function(e){
    e.preventDefault()
    if (confirm('Are you sure?')==true) {
        $(e.target).closest('form').submit()
    }
})
</script>

@endsection
