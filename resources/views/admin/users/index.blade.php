@extends('admin.homepage')
@section('content')
<h3>{{__('news.plural.user')}}</h3>
<hr>
<div>
    <a href="{{route('admin.users.create')}}">
        <button type="button" class="btn btn-primary">{{__('news.user.create')}}</button>
    </a>
</div>
<div>
<table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">{{__('news.user.name')}}</th>
            <th scope="col">{{__('news.user.email')}}</th>
            <th scope="col">{{__('news.plural.role')}}</th>
            <th scope="col">{{__('news.plural.created')}}</th>
            <th scope="col">{{__('news.plural.updated')}}</th>
            <th scope="col">{{__('news.plural.deleted')}}</th>
            <th scope="col">{{__('news.plural.action')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $users as $key=>$user )
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->role}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                <td>{{$user->deleted_at?$user->deleted_at:''}}</td>
                <td class="d-flex">
                    @if ($user->deleted_at)
                    @else
                    <a href="{{route('admin.users.edit',$user->id)}}">
                        <button type="button" class="btn btn-warning">{{__('news.user.edit')}}</button>
                    </a>
                    <form action="{{route('admin.users.destroy',$user->id)}}" method="POST">
                        @csrf
                        @method('delete')
                        <button onclick="" type="submit" class="deleteBtn btn btn-danger">{{__('news.user.delete')}}</button>
                    </form>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('script')
<script>
$('.deleteBtn').click(function(e){
    e.preventDefault()
    if (confirm('{{__('news.plural.confirm')}}')) {
        $(e.target).closest('form').submit()
    }
})
</script>
@endsection
