@extends('admin.homepage')
@section('content')
<h3>{{__('news.plural.user')}}</h3>
<hr>
<div>
    <h4>{{__('news.user.profile')}}</h4>
    <a>
        <button type="button" onclick="history.back()" class="btn btn-warning">Cancle</button>
    </a>
    <form action="{{route('admin.users.update',$user->id)}}" method="post">
        @method('put')
        @csrf
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{__('news.user.name')}}</label>
            <input type="string" class="form-control" id="exampleInputEmail1" name="name" value="{{old('name')?old('name'):$user->name}}">
            @foreach ($errors->get('name') as $nameMessage)
                <div class="text-danger">{{$nameMessage}}</div>
            @endforeach
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{__('news.user.email')}}</label>
            <input disabled type="email" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" name="email" value="{{$user->email}}">
            @foreach ($errors->get('email') as $emailMessage)
                <div>{{$emailMessage}}</div>
            @endforeach
        </div>
        @can('only-admin',Auth::id())
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">{{__('news.plural.role')}}</label>
            <select name="role" class="form-select" aria-label="Default select example">
                <option value="1" {{$user->isAdmin()?'selected':''}}>Admin</option>
                <option value="2" {{$user->isWritter()?'selected':''}}>Writter</option>
                <option value="3" {{$user->isUser()?'selected':''}}>User</option>
            </select>
            @foreach ($errors->get('role') as $roleMessage)
                <div>{{$roleMessage}}</div>
            @endforeach
        </div>
        @endcan
        <button type="submit" class="btn btn-primary">{{__('news.user.send')}}</button>
    </form>
</div>
<hr></hr>
<div>
    <h4>{{__('news.plural.pass-change')}}</h4>
    <form action="{{route('admin.users.update',$user->id)}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="exampleInputPassword1">{{__('news.user.old-pass')}}</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Old Password" name="old_password">
            @foreach ($errors->get('old_password') as $oldPasswordMessage)
                <div class="text-danger">{{$oldPasswordMessage}}</div>
            @endforeach
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">{{__('news.user.new-pass')}}</label>
            <input type="password" class="form-control" id="exampleInputPassword2" placeholder="New Password" name="password">
            @foreach ($errors->get('password') as $passwordMessage)
                <div class="text-danger">{{$passwordMessage}}</div>
            @endforeach
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">{{__('news.user.new-pass-cf')}}</label>
            <input type="password" class="form-control" id="exampleInputPassword3" placeholder="New Password Confirm" name="password_confirmation">
        </div>
        <button type="submit" class="btn btn-primary">{{__('news.user.send')}}</button>
    </form>
</div>
@endsection
