@extends('admin.homepage')
@section('content')
<h4>{{__('news.plural.received')}}</h4>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">{{__('user.plural.content')}}</th>
      <th scope="col">{{__('user.plural.readed')}}</th>
      <th scope="col">{{__('user.plural.created')}}</th>
    </tr>
  </thead>
  <tbody>
      @foreach ($receivedNotifications as $key=>$notification)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$notification['data']['msg']}}</td>
          <td>{{$notification['read_at']}}</td>
          <td>{{$notification['created_at']}}</td>
        </tr>
      @endforeach
  </tbody>
</table>
@can('only-admin',Auth::user())
<hr></hr>
<h4>{{__('news.plural.sent')}}</h4>
<a href="{{route('admin.notification.create')}}">
  <button type="button" class="btn btn-primary">{{__('news.plural.send-btn')}}</button>
</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">{{__('news.plural.type')}}</th>
      <th scope="col">{{__('news.plural.receiver')}}</th>
      <th scope="col">{{__('news.plural.title')}}</th>
      <th scope="col">{{__('news.plural.content')}}</th>
      <th scope="col">{{__('news.plural.created')}}</th>
      <th scope="col">{{__('news.plural.send-date')}}</th>
      <th scope="col">{{__('news.plural.status')}}</th>
      <th scope="col">{{__('news.plural.action')}}</th>
    </tr>
  </thead>
  <tbody>
      @foreach ($notifications as $key=>$notification)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$notification->type}}</td>
          <td>{{$notification->receiver_id!==null&&$notification->type=='other'?count($notification->receiver_id):$notification->type}}</td>
          <td>{{$notification->title}}</td>
          <td>{{$notification->msg}}</td>
          <td>{{$notification->created_at}}</td>
          <td>{{$notification->send_date}}</td>
          <td>{{$notification->status}}</td>
          <td class="d-flex">
            @if ($notification->status=='pending')
              <a href="{{route('admin.notification.edit',$notification->id)}}">
                <button type="button" class="btn btn-warning">{{__('news.user.edit')}}</button>
              </a>
            @endif
            <form method="post" action="{{route('admin.notification.destroy',$notification->id)}}">
              @method('delete')
              @csrf
              <button type="submit" class="deleteBtn btn btn-danger">{{__('news.user.delete')}}</button>
            </form>
          </td>
        </tr>
      @endforeach
  </tbody>
</table>
@endcan
<script>
$('.deleteBtn').click(function(e){
    e.preventDefault()
    if (confirm('{{__('news.plural.confirm')}}')) {
        $(e.target).closest('form').submit()
    }
})
</script>
@endsection
