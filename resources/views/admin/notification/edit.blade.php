@extends('admin.homepage')
@section('content')
<h3>{{__('news.plural.notification')}}</h3>
<hr>
<div>
    <form action="{{route('admin.notification.update',$notification->id)}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="exampleInputEmail1">{{__('user.plural.title')}}</label>
            <input type="text" name="title" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$notification->title}}">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">{{__('user.plural.msg')}}</label>
            <input type="text" name="msg" class="form-control" id="exampleInputPassword1" value="{{$notification->msg}}">
        </div>
        <label for="option">{{__('news.plural.choose-receiver')}}</label>
        <div class="d-flex pb-3" id="option">
            <select id="users" name="type" class="form-select">
                <option id="all" value="all" {{$notification->type=='all'?'selected':''}}>{{__('user.plural.all')}}</option>
                <option id="writer" value="writer" {{$notification->type=='writer'?'selected':''}}>{{__('user.plural.writer')}}</option>
                <option id="user" value="user" {{$notification->type=='user'?'selected':''}}>{{__('user.plural.only-user')}}</option>
                <option id="other" value="other" {{$notification->type=='other'?'selected':''}}>{{__('user.plural.other')}}</option>
            </select>
            <br>
            <select id="select-user" name="receiver_id[]" class="form-select w-50" disabled>
                @foreach ($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{__('user.plural.choose-send-date')}}</label>
            <input name="send_date" type="datetime-local" value="{{$notification->send_date}}"/>
        </div>
        <button type="submit" class="btn btn-primary">{{__('user.user.send')}}</button>
    </form>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
    $("#users").change(function(){
        if($("#other:selected").length>0){
            $('#select-user').removeAttr('disabled')
        }
        if($("#all:selected").length>0){
            $('#select-user').attr('disabled','disabled')
        }
        if($("#writer:selected").length>0){
            $('#select-user').attr('disabled','disabled')
        }
        if($("#user:selected").length>0){
            $('#select-user').attr('disabled','disabled')
        }
    })
})
$('#select-user').select2({
    multiple:true,
});
</script>
@endsection
