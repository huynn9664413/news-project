@extends('admin.homepage')
@section('content')
<h3>{{__('news.plural.post')}}</h3>
<hr>
<div>
    <a href="{{route('admin.post.create')}}">
        <button type="button" class="btn btn-primary">{{__('news.user.create')}}</button>
    </a>
</div>
<form action="{{route('admin.post.index')}}" method="get">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" name="search">
        @can('only-admin',Auth::user())
            <select class="custom-select" name="author">
                <option value="none">{{__('news.plural.author')}}...</option>
                @foreach ($authors as $author)
                    <option value="{{$author->id}}">{{$author->name}}</option>
                @endforeach
            </select>
        @endcan
        <select class="custom-select" name="category">
            <option value="none">{{__('news.plural.category')}}...</option>
            @foreach ($categories as $category)
                <option value="{{$category->id}}">{{$category->category}}</option>
            @endforeach
        </select>
        <select class="custom-select" name="status">
            <option value="none">{{__('news.plural.status')}}...</option>
            @foreach ($status as $status)
                <option value="{{$status}}">{{$status}}</option>
            @endforeach
        </select>
        <div class="input-group-append">
            <button type="submit" class="btn btn-outline-secondary">{{__('user.plural.status')}}</button>
        </div>
    </div>
</form>
<div>
    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">{{__('news.plural.title')}}</th>
            <th scope="col">{{__('news.plural.author')}}</th>
            <th scope="col">{{__('news.plural.category')}}</th>
            <th scope="col">{{__('news.plural.send-date')}}</th>
            <th scope="col">{{__('news.plural.status')}}</th>
            <th scope="col">{{__('news.plural.created')}}</th>
            <th scope="col">{{__('news.plural.updated')}}</th>
            <th scope="col">{{__('news.plural.action')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $posts as $key=>$post )
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$post->title}}</td>
                <td>{{$post->user->name}}</td>
                <td>{{$post->category->category}}</td>
                <td>{{$post->publish_at}}</td>
                <td>{{$post->status}}</td>
                <td>{{$post->created_at}}</td>
                <td>{{$post->updated_at}}</td>
                <td class="d-flex">
                  <a href="{{route('admin.post.show',$post->id)}}">
                      <button type="button" class="btn btn-info">{{__('news.plural.show')}}</button>
                  </a>
                  @if ($post->publish_at>=now())
                  <a href="{{route('admin.post.edit',$post->id)}}">
                      <button type="button" class="btn btn-warning">{{__('news.user.edit')}}</button>
                  </a>
                  @endif
                  <form action="{{route('admin.post.destroy',$post->id)}}" method="POST">
                      @csrf
                      @method('delete')
                      <button type="submit" class="deleteBtn btn btn-danger">{{__('news.user.delete')}}</button>
                  </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('script')
<script>
$('.deleteBtn').click(function(e){
    e.preventDefault()
    if (confirm('{{__('user.plural.confirm')}}')) {
        $(e.target).closest('form').submit()
    }
})
</script>
@endsection
