@extends('admin.homepage')
@section('styles')

@endsection
@section('content')
<h3>{{__('news.plural.post')}}</h3>
<hr>
<div class="d-flex justify-content-end">
<a href="{{route('admin.post.edit',$post->id)}}">
    <button type="button" class="btn btn-warning">{{__('news.user.edit')}}</button>
</a>
</div>
<div class="container">
    <div class="d-flex justify-content-center">
        <strong><h3>{{$post->title}}</h3></strong>
    </div>
    <div class="d-flex justify-content-center">
        <p>
            {{__('news.plural.post-subtitle',
                [
                'name'=>$post->user->name,
                'category'=>$post->category->category,
                'time'=>$post->created_at
                ]
            )}}
        </p>
    </div>
    <div class="d-flex justify-content-center">
        <img src="{{$post->front_page_image_path}}" alt="">
    </div>
    <div class="d-flex justify-content-center">
        <div>{!!$post->content!!}</div>
    </div>
</div>
@endsection
