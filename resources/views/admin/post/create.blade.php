
@extends('admin.homepage')
@section('styles')
<style>
#showImage{
    max-height: 300px;
}
.image{
    max-height: 200px;
}
#tags{
    width: 100%;
}
</style>
@endsection
@section('content')
<h3>{{__('news.user.post')}}</h3>
<hr>
<div>
<form action="{{route('admin.post.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{__('news.plural.title-image')}}</label>
            <select id="titleImage" name="front_page_image_path" aria-label="Default select example">
                <option value="">...</option>
                @foreach ($images as $image)
                <option value="{{$image->path}}">{{$image->original_name}}</option>
                @endforeach
            </select>
            <div>
                <img id="showImage" src="" alt="">
            </div>
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{__('news.plural.title')}}</label>
            <input type="string" class="form-control" name="title" value="{{old('title')}}">
            @foreach ($errors->get('title') as $titleMessage)
                <div>{{$titleMessage}}</div>
            @endforeach
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">{{__('news.plural.category')}}</label>
            <select name="category_id" class="form-select" aria-label="Default select example">
                <option value="">...</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->category}}</option>
                @endforeach
            </select>
            @foreach ($errors->get('category') as $categoryMessage)
                <div>{{$categoryMessage}}</div>
            @endforeach
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{__('news.plural.image')}}</label>
            <i>({{__('news.plural.use-image-instruction')}})</i>
            <div>
            @foreach ($images as $image)
                <img src="{{$image->path}}" class="image" alt="">
            @endforeach
            </div>

        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{__('news.plural.content')}}</label>
            <textarea name="content" id="content">{{old('content')}}
            </textarea>
            @foreach ($errors->get('content') as $contentMessage)
                <div>{{$contentMessage}}</div>
            @endforeach
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{__('news.plural.tags')}}</label>
            <div>
                <strong><i>{{__('news.plural.choose-tags')}}</i></strong>
                <select id="tags" name="tags[]" class="form-select" aria-label="Default select example"></select>
            </div>
            <i>{{__('news.plural.or')}}</i>
            <div>
                <strong><i>{{__('news.user.create-tags')}}</i></strong>
                <div class="text-align-center input-group mb-3">
                    <input id="createTags" type="text" name="name" placeholder="Tags..." aria-label="Recipient's username" aria-describedby="button-addon2">
                    <button class="btn btn-outline-secondary" type="button" id="createBtn">{{__('news.user.create')}}</button>
                </div>
            </div>

        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{__('news.plural.choose-send-date')}}</label>
            <input name="publish_at" type="datetime-local" value="{{date('Y-m-d H:i')}}"/>
        </div>
        <button type="submit" class="btn btn-primary">{{__('news.user.send')}}</button>
        <a href="{{route('admin.post.index')}}">
            <button type="button" class="btn btn-warning">{{__('news.user.cancel')}}</button>
        </a>
</form>
</div>
@endsection
@section('script')
<script src="https://cdn.ckeditor.com/ckeditor5/41.3.1/classic/ckeditor.js"></script>
<script>
    $(document).ready(function(){
        fetchTags();
        ClassicEditor
            .create( document.querySelector( '#content' ),
            {
                ckfinder:
                {
                    uploadUrl:"",
                }
            })
            .catch( error => {
                console.error( error );
            } );
        $('#tags').select2({
            multiple:true,
        });
    })



    function removeTags(){
        $("#tags").empty();
    }

    function fetchTags(){
        fetch('{{route("showTags")}}')
        .then(r=>r.json())
        .then(dat=>$.each(dat,function(index,value){
            var option = '<option value="'+value['id']+'">'+value['name']+'</option>'
            $('#tags').append(option);
        }));
    }


    $('#titleImage').on('change', function() {
        console.log(this.value)
        $('#showImage').attr('src',this.value)
    });

    $('#createBtn').click(function(){
        removeTags();
        fetch('{{route("createTags")}}', {
            method: "POST",
            body: JSON.stringify({
                _token: '{{csrf_token()}}',
                name: $('#createTags').val(),
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        });
        fetchTags();
    })

</script>
@endsection
